"use strict";
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId;

var DataHolderSchema = new Schema({  
  parent: {type: ObjectId},  
  data: {type: Buffer}
});

const DataHolderModel = mongoose.model('DataHolder', DataHolderSchema);

module.exports = DataHolderModel;
