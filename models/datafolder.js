"use strict";
const Types = require("../public/js/types.js");
const DataHolder = require("./dataholder.js");
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId;

const DataFolderSchema = new Schema({  
  parent: { type: ObjectId },  
  items: { 
    type: Map,
    of: { type: ObjectId },    
    default: {}
  },  
});

const NamedDataFolderSchema = new Schema({  
  parent: { type: ObjectId },  
  items: { 
    type: Map,
    of: { 
      filename: { type: String },
      dataHolderType: { type: Number },
      dataHolder: { type: ObjectId },
      uploader: { type: ObjectId, ref: 'User' },
      uploadDate: { type: Date },
      size: { type: Number }
    },
    default: {}
  },  
});

DataFolderSchema.methods.setData = function (name, data, callback){
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = this.items.get(key);
  //console.log(dataHolderId);              
  if (dataHolderId != null){
    DataHolder.findById(dataHolderId, (err, dataHolder)=>{
      if (err){
        res.writeHead(500, {'Content-Type': 'html/text'});      
        return callback("Server error", null); 
      }else{
        dataHolder.data = data;
        dataHolder.increment();
        dataHolder.save();        
        return callback(null, "Success"); 
      }
    });
  } else {        
    let dataHolderData = {
      data: data,
      owner: this._id
    }
    let dataHolder = new DataHolder(dataHolderData);               
    this.items.set(key, dataHolder._id);
    this.save();
    dataHolder.save();    
    return callback(null, "Success");       
  }
}


DataFolderSchema.methods.getData = async function (name){
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = this.items.get(key);
  if (dataHolderId != null){
    let dataHolder = await DataHolder.findById(dataHolderId);
    return dataHolder.data.toString("utf8");     
  } else { 
    return null;
  }
}

DataFolderSchema.methods.getNamedDataFolder = async function (name){
  let key = Buffer.from(name).toString('base64');
  let dataItemId = this.items.get(key);
  if (dataItemId != null){
    let namedDataFolder = await NamedDataFolderModel.findById(dataItemId);
    return namedDataFolder;
  } else { 
    return null;
  }
}

DataFolderSchema.methods.setDataFolder = async function (name, dataFolder){
  let key = Buffer.from(name).toString('base64');
  let dataItemId = this.items.get(key);
  if (dataItemId != null){
    throw new Error("name already exists");
  } else {              
    this.items.set(key, dataFolder._id);
    this.save();
  }
}

DataFolderSchema.methods.getItemByPath = async function (path, makedirs=false) {
  //console.log("DataFolder.getItemByPath");
  //console.log(path);
  let name = path.shift();
  let key = Buffer.from(name).toString('base64');
  let dataItemId = this.items.get(key);
  let dataItem = await DataHolder.findById(dataItemId);
  if (dataItem) {
    if (path.length == 0){
      return dataItem;
    } else {
      throw new Error("File " + name + " is is not folder.");
    }
  }
  if (!dataItem) dataItem = await NamedDataFolderModel.findById(dataItemId);
  if (!dataItem && makedirs)
  {
    dataItem = new NamedDataFolderModel({ parent: this._id });        
    await dataItem.save();
    this.items.set(key, dataItem._id);
    await this.save();
  } else if (!dataItem){
    //console.log("missing dataItem " + name );
    return null;
  }
  if (dataItem) {
    if (path.length == 0){
      return dataItem;
    } else {
      return dataItem.getItemByPath(path);
    }
  }
  console.log("not found");
  return null;
}

DataFolderSchema.methods.setDatas = function (datas, callback){
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = this.items.get(key);
  //console.log(dataHolderId);              
  if (dataHolderId != null){
    DataHolder.findById(dataHolderId, (err, dataHolder)=>{
      if (err){
        res.writeHead(500, {'Content-Type': 'html/text'});      
        return callback("Server error", null); 
      }else{
        dataHolder.data = data;
        dataHolder.increment();
        dataHolder.save();        
        return callback(null, "Success"); 
      }
    });
  } else {        
    let dataHolderData = {
      data: data,
      owner: this._id
    }
    let dataHolder = new DataHolder(dataHolderData);               
    this.items.set(key, dataHolder._id);
    this.save();
    dataHolder.save();    
    return callback(null, "Success");       
  }
}

DataFolderSchema.methods.deleteContent = async function (){  
  let values = Array.from(this.items.values());
  for (let i in values){
    let holderId = values[i];
    let holder = await DataHolder.findById(holderId);    
    if (holder) holder.remove();
  }  
}

DataFolderSchema.methods.deleteFolder = async function () {
  await this.deleteContent();
  this.remove();
}

NamedDataFolderSchema.methods.deleteItem = async function (name) {
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = null;
  let dataItem = this.items.get(key);
  if (dataItem) dataHolderId = dataItem.dataHolder;
  if (!dataHolderId) dataHolderId = dataItem.dataholder;    /* fixes a bug with capitalization in mongo */
  if (dataItem.dataHolderType == Types.DataHolderTypes.FileHolder){
    let dataHolder = await DataHolder.findById(dataHolderId); 
    if (dataHolder){
      dataHolder.remove();
      this.items.delete(key);
      await this.save();       
      return true; 
    } else {
      this.items.delete(key);
      await this.save();   
      throw new Error("file not found");
    }
  } else {
    let namedDataFolder = await NamedDataFolderModel.findById(dataHolderId); 
    if (namedDataFolder){
      namedDataFolder.deleteFolder();
      this.items.delete(key);
      await this.save();       
      return true;  
    } else {
      throw new Error("Folder not found");
    }
  }
}

NamedDataFolderSchema.methods.removeItem = async function (name) {
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = null;
  let dataItem = this.items.get(key);
  if (dataItem) dataHolderId = dataItem.dataHolder;
  if (!dataHolderId) dataHolderId = dataItem.dataholder;    /* fixes a bug with capitalization in mongo */
  if (dataItem.dataHolderType == Types.DataHolderTypes.FileHolder){
    let dataHolder = await DataHolder.findById(dataHolderId); 
    if (dataHolder){
      //dataHolder.remove();
      this.items.delete(key);
      await this.save();       
      return true; 
    } else {
      this.items.delete(key);
      await this.save();   
      throw new Error("file not found");
    }
  } else {
    let namedDataFolder = await NamedDataFolderModel.findById(dataHolderId); 
    if (namedDataFolder){
      //namedDataFolder.deleteFolder();
      this.items.delete(key);
      await this.save();       
      return true;  
    } else {
      throw new Error("Folder not found");
    }
  }
}

NamedDataFolderSchema.methods.insertExistingHolder = async function (name, holder, uploaderId){
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = null;
  let dataItem = this.items.get(key);
  if (dataItem) dataHolderId = dataItem.dataHolder;                               
  if (dataHolderId != null){
    throw new Error("File with that name already exists.")
  } else {          
    let dataHolderType = holder.items == null ? Types.DataHolderTypes.FileHolder : Types.DataHolderTypes.FolderHolder;
    let folderItemData = { 
      filename: name, 
      dataHolder: holder._id, 
      dataHolderType: dataHolderType,
      uploader: uploaderId,
      uploadDate: new Date()
    }         
    holder.parent = this._id;
    await holder.save();
    this.items.set(key, folderItemData);
    await this.save();
    return true;
  }
}

NamedDataFolderSchema.methods.setData = async function (name, data, uploaderId){
  let key = Buffer.from(name).toString('base64');
  let dataHolderId = null;
  let dataItem = this.items.get(key);
  if (dataItem) dataHolderId = dataItem.dataHolder;                               
  if (dataHolderId != null){
    let dataHolder = await DataHolder.findById(dataHolderId);//, (err, dataHolder)=>{
    dataHolder.data = data;
    dataHolder.increment();
    await dataHolder.save();        
    return dataHolder;
  } else {        
    let dataHolderData = {
      data: data,
      owner: this._id
    }
    let dataHolder = new DataHolder(dataHolderData);  
    let folderItemData = { 
      filename: name, 
      dataHolder: dataHolder._id, 
      dataHolderType: Types.DataHolderTypes.FileHolder,
      uploader: uploaderId,
      uploadDate: new Date()
    }              
    this.items.set(key, folderItemData);
    await this.save();
    await dataHolder.save();   
    return dataHolder;
  }
}

NamedDataFolderSchema.methods.createFolder = async function (name, uploaderId) {
  let key = Buffer.from(name).toString('base64');
  let dataItem = this.items.get(key);
  if (dataItem) {
    throw new Error("Name already exists");
  } else {    
    let namedDataFolder = new NamedDataFolderModel({ parent: this._id });
    let folderItemData = { 
      filename: name, 
      dataHolder: namedDataFolder._id, 
      dataHolderType: Types.DataHolderTypes.FolderHolder,
      uploader: uploaderId,
      uploadDate: new Date()
    }    
    this.items.set(key, folderItemData);
    await this.save();
    await namedDataFolder.save();
    return namedDataFolder;
  }
}

NamedDataFolderSchema.methods.getItemByPath = async function (path, makedirs=null) {
  //console.log("NamedDataFolderSchema.getItemByPath");
  
  let name = "";
  while (name == "" && path.length > 0) {
    name = path.shift();
  }
  let key = Buffer.from(name).toString('base64');
  let dataItem = this.items.get(key);

  if (!dataItem) {
    if (makedirs)
    {
      await this.createFolder(name, makedirs.userId);
      dataItem = this.items.get(key);
    } else {
      //console.log("missing dataItem " + name );
      return null;
    }
  } 
  if (dataItem.dataHolderType == Types.DataHolderTypes.FileHolder) {
    let dataHolder = await DataHolder.findById(dataItem.dataHolder);
    if (!dataHolder) return null;
    if (path.length == 0){
      return dataHolder;
    } else {
      throw new Error("File " + name + " is is not folder.");
    }
  }
  if (dataItem.dataHolderType == Types.DataHolderTypes.FolderHolder) {
    let dataFolder = await NamedDataFolderModel.findById(dataItem.dataHolder);
    if (!dataFolder) return null;
    if (path.length == 0){
      return dataFolder;
    } else {
      return dataFolder.getItemByPath(path, makedirs);
    }
  }
  return null;
}



NamedDataFolderSchema.methods.deleteContent = async function (){  
  let values = Array.from(this.items.values());
  for (let i in values) {
    let holder = values[i];

    if (holder.dataHolderType == null || holder.dataHolderType == Types.DataHolderTypes.FileHolder){
      let dataHolder = await DataHolder.findById(holder.dataHolder);
      if (dataHolder) dataHolder.remove();  
    } else {
      let dataFolder = await NamedDataFolderModel.findById(holder.dataHolder);
      console.log("remove folder");
      console.log(holder.filename);
      dataFolder.deleteFolder();
    }        
  }  
}

NamedDataFolderSchema.methods.deleteFolder = async function (){
  await this.deleteContent();
  this.remove();
}

const DataFolderModel = mongoose.model('DataFolder', DataFolderSchema);
const NamedDataFolderModel = mongoose.model('NamedDataFolder', NamedDataFolderSchema);

module.exports = {
  DataFolderModel: DataFolderModel,
  NamedDataFolderModel: NamedDataFolderModel
}
