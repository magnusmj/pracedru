const DataHolder = require("./dataholder.js");
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId;

const BlogSchema = new Schema({  
  title: { type: String, required: true },
  date: { type: Date, required: true },
  snippet: { type: String, required: true },
  hashtags: [{ type: String }],
  article: { type: ObjectId, ref: 'DataHolder', required: true  },
  published: { type: Boolean, default: true }
});


const BlogModel = mongoose.model('Blog', BlogSchema);

module.exports = BlogModel;
