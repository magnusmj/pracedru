"use strict";

let cacheVersion = "1.1.13";
let mobile = null;
let dismisPopupFunction = null;
let cacheUpdateDict = {}
let poppingState = false;
let themes = [
  {name: "Default", css: "defaultcss"},
  {name: "Dark", css: "darkcss"},
  {name: "Rouge", css: "rougecss"},
  {name: "Azure", css: "azurecss"}
];
let fileIcons = {
  "txt": "file-txt.svg",
  "pdf": "file-pdf.svg",
  "pptx": "file-pptx.svg",
  "ppt": "file-pptx.svg",
  "docx": "file-word.svg",
  "doc": "file-word.svg",
  "odt": "file-word.svg",
  "xlsx": "file-excel.svg",
  "xls": "file-excel.svg",
  "ods": "file-excel.svg",
  "png": "file-img.svg",
  "jpg": "file-img.svg",
  "jpeg": "file-img.svg",
  "svg": "file-img.svg",
  "gif": "file-img.svg",
  "mp3": "file-music.svg",
  "mpg3": "file-music.svg",
  "mp4": "file-video.svg",
  "mpg4": "file-video.svg",
  "wav": "file-music.svg",
  "zip": "file-zip.svg",
  "7z": "file-zip.svg",
  "tar": "file-zip.svg",  
  "quill": "file-note.svg",  
  "default": "file-txt.svg",
}

function pushState(state){
  if (!poppingState){
    let title = Object.keys(ViewTypes)[state.view];
    //console.log(title);
    history.pushState(state, title);
    gtag('event', 'page_view', {
      page_title: title,
      page_path: window.location.pathname,
      send_to: 'UA-178509530-2'
    });
  }
}

window.onpopstate = function (e) {
  console.log(e);
  poppingState = true;
  try {
    switch (e.state.view){
      case ViewTypes.Home:
        onHomeClicked();
        break;
      case ViewTypes.Business:
        onBusinessClicked();
        break;
      case ViewTypes.Communication:
        openChatState(e.state);
        return; // avoiding poppingState to make the channel load according to the state
      case ViewTypes.TaskManagement:
        onTasksClicked();
        break;
      case ViewTypes.SettingsUser:
        onContextUserSettingsClicked();
        break;
      case ViewTypes.SettingsOrg:
        onContextOrgSettingsClicked();
        break;
      case ViewTypes.SettingsJambo:
        onContextJamboSettingsClicked();
        break;
    }
  } catch(e){
  
  }  
  poppingState = false;
};

function checkVisible(elm) {
  let rect = elm.getBoundingClientRect();
  let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
  return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}

function setStyleSheetDisabled(cssName, disabled){
  let cssItem = document.getElementById(cssName);
  if (cssItem) cssItem.disabled = disabled;
}

function onAcceptCookiesClicked(){
  localStorage.cookiesAccepted = JSON.stringify({ functional: true });
  closePopup();
}

function rejectCookies(){
  localStorage.removeItem("cookiesAccepted");
  return rejectCookies;
}

function handleSideNavClicked(btn){
  if (!btn) return;
  let selected_side_navs = document.getElementsByClassName("side_nav_button_selected");
  for(var i=0; i< selected_side_navs.length; i++) { 
    selected_side_navs[i].classList.remove("side_nav_button_selected");
  }  
  btn.classList.add("side_nav_button_selected");
}

function removeFileExtension(filename){
  if (filename.indexOf(".") != -1){
    let filenameItems = filename.split(".");
    filenameItems.pop();
    filename = "";
    for (let i in filenameItems){
      filename += (i == 0 ? "" : ".") + filenameItems[i];
    }
  }
  return filename;
}


/**
 * Sets the theme of jambo by enabling and disabling the css that corresponds to the theme.
 *
 * @param {string} theme The name of the theme to be set.
 * @return {null}
 */
function setTheme(css){ 
  if (themes.find(theme => theme.css == css)) { // .indexOf(theme) >= 0){
    setStyleSheetDisabled(css, false);
    setTimeout(()=>{
      for (let i in themes){
        let theme = themes[i];
        if (theme.css != css){
          setStyleSheetDisabled(theme.css, true);  
        }
      }  
      let colorThemeDiv = document.createElement("div");
      colorThemeDiv.classList.add("app_theme")
      document.body.appendChild(colorThemeDiv);      
      const style = getComputedStyle(colorThemeDiv);
      let color = style.backgroundColor;
      colorThemeDiv.remove();
      document.querySelector('meta[name="theme-color"]').setAttribute('content',  color);
    }, 200); 
  }  
}

function delayedClearMessage(msg){
  let messageItem = document.getElementById("message");
  messageItem.innerHTML = msg;
  setTimeout(() => {     
    messageItem.innerHTML = "";
  }, 5000);
}

function getObjectById(objectList, id){
  for (let i in objectList){
    let obj = objectList[i];
    if (id === obj._id) return obj;
  }
  return null;
}

function onPolicyClicked(){
  //window.location = "policy.html";
  var win = window.open("policy.html", '_blank');
  win.focus();
}

/**
 * Fills the content from the path given into content element given.
 *
 * @param {string} path The path where the content is to be gotten from.
 * @param {function} callback The function to be called when the content is filled in.
 * @param {string} contentElementId Optional id of the content element to be used for
 * inserting the content.
 * @return {null}
 */
function fillContent(path, callback=null, contentElementId="content"){
  let contentElement = document.getElementById(contentElementId);
  get(path, {}, (xmlhttp) => {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {      
      contentElement.innerHTML = xmlhttp.responseText;    
      if (callback) callback();
      let content_div = document.getElementById("content_div");
      //content_div.style.height = (window.innerHeight - 80) + 'px';
    } else if(xmlhttp.status == 401){
      contentElement.innerHTML = "";
      console.log(xmlhttp.responseText);
    }
  });
}

/**
 * Custom date to short string function
 *
 * @param {Date} d The date to be formatted into a string.
 * @return {string} A short representation of a date as a string.
 */
function Date2String(d){
  const ye = new Intl.DateTimeFormat('en-US', { year: 'numeric' }).format(d);
  const mo = new Intl.DateTimeFormat('en-US', { month: 'short' }).format(d);
  const da = new Intl.DateTimeFormat('en-US', { day: '2-digit' }).format(d);
  return mo + ' ' + da + ' ' + ye;
}


/**
 * Helper function for setting cookies.
 *
 * @param {string} cname The name of the cookie to set.
 * @param {string} cvalue The value of the cookie to set.
 * @param {string} exdays The expiry length in days, for the cookie.
 * @return {null}
 */
function setCookie(cname, cvalue, exdays) {
  let d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  let expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/" + ";strict";
}

/**
 * Helper function for getting cookies.
 *
 * @param {string} cname The name of the cookie to get.
 * @return {string} The cookie value if it is existing and and empty string if it is not.
 */
function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

/**
 * General POST function for posting data to the backend through XMLHttpRequest.
 *
 * @param {string} path The path of the data to post.
 * @param {string} body The body contents for the post.
 * @param {function} callback The function that is to be assigned to XMLHttpRequest.onreadystatechange. 
 * @return {null} 
 */
function post(path, body, callback){
  let xhttp = new XMLHttpRequest();
  let data = JSON.stringify(body);
  xhttp.onreadystatechange = function () { callback(this); }
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
  xhttp.send(data);
}

function CacheReturnObject(data) {
  this.readyState = 4;
  this.status = 200;
  this.responseText = data;
  this.onreadystatechange = null;  
}

function clearCache(){
  console.log("new version detected clearing cache");
  for (let key in localStorage){
    if (key.indexOf("cache-") == 0) localStorage.removeItem(key);
  }
}

/**
 * General Get function for getting backend data through XMLHttpRequest.
 *
 * @param {string} path The path of the data to get.
 * @param {dict} args The arguments to be sent as querydata.
 * @param {function} callback The function that is to be assigned to XMLHttpRequest.onreadystatechange.
 * @param {boolean} cache If false the get cache will be ignored. Otherwise caches the data for faster
 * delivery. Will not cache if any arguments are set. 
 * @return {null} 
 */
function get(path, args, callback, cache=true){
  if (localStorage.cacheVersion != cacheVersion){
    clearCache();
    localStorage.cacheVersion = cacheVersion;
  }
  let cacheReturned = false;
  let cachePath = "cache-" + path;
  let shouldCache = cache && Object.keys(args).length == 0;
  if (location.hostname == "localhost" || location.hostname.indexOf("testing") !=-1) shouldCache = false;
  if (shouldCache) {    
    if (cachePath in localStorage) {
      let cro = new CacheReturnObject(localStorage[cachePath]);
      cacheReturned = true;      
      //console.log("cache returned");
      if (callback){
        try {
          callback(cro);
        } catch (err) {
          console.log(err)
        }        
      } 
    }
  }
  let req = new XMLHttpRequest();
  let urlargs = "";
  for (let key of Object.keys(args)){
  	if (urlargs.length == 0) urlargs += "?";
  	else urlargs += "&";
  	urlargs += key + "=" + args[key];
  }
  if (cacheReturned) {
    req.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {    
        localStorage[cachePath] = this.responseText;
        //console.log("cache updated")
      }
    }
  } else if (shouldCache) {
    req.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {    
        localStorage[cachePath] = this.responseText;
        //console.log("cache created")        
      }
      let cro = new CacheReturnObject(localStorage[cachePath]);
      cro.status = this.status;
      cro.readyState = this.readyState;
      
      if (callback) callback(cro);   
    }
  } else {  
    req.onreadystatechange = function() { callback(this); } ;
  }
  req.open("GET", path + urlargs, true);
  req.send();
}

/**
 * This creates a foldout item ui element with the given data and content creator.
 *
 * @param {dict} foldoutData The data to be contained in the foldout (filled with the content creator).
 * @param {function} contentCreator The function that creates the foldout ui element based on the foldoutData.
 * @return {div} The fold out ui item. 
 */
function createFoldoutItem(foldoutData, contentCreator, caption=null){
  let foldoutItem = document.createElement("div");
  let foldoutButtonItem = document.createElement("div");
  let foldoutButtonIconItem = document.createElement("div");
  let foldoutButtonIconHolderItem = document.createElement("div");
  foldoutItem.appendChild (foldoutButtonItem);
  foldoutButtonItem.style.margin = "0.8em auto";
  foldoutButtonItem.classList.add("rounded_shade_button");
  foldoutButtonItem.classList.add("filled_light_control");
  foldoutButtonItem.classList.add("tabled");
  foldoutButtonItem.classList.add("extra_wide");
  foldoutButtonItem.classList.add("medium_bold");
  foldoutButtonItem.classList.add("text_heading_blue");
  foldoutButtonIconHolderItem.appendChild(foldoutButtonIconItem);
  foldoutButtonIconHolderItem.style.position = "relative";  
  foldoutButtonIconItem.style.position = "absolute";  
  foldoutButtonIconItem.style.right = "0";
  foldoutButtonIconItem.innerHTML = "▼";
  
  foldoutButtonItem.appendChild(foldoutButtonIconHolderItem);
  foldoutButtonItem.append(caption ? caption : foldoutData.name);
  let foldoutContentItem = null;
  foldoutButtonItem.onclick = (e) => {
    let updown = foldoutButtonIconItem.innerHTML;
    if (updown == "▼") {
      foldoutButtonIconItem.innerHTML = "▲";
      foldoutContentItem = contentCreator(foldoutData, foldoutItem);
      foldoutItem.classList.add("grow_up");
      foldoutItem.appendChild(foldoutContentItem);
    } else {
      foldoutItem.classList.remove("grow_up");
      foldoutButtonIconItem.innerHTML = "▼";
      foldoutContentItem.remove();
    }        
  }
  return foldoutItem;
}



function createPopup(popupContentPath, title, finalCallback){  
  get("content/popup.html", {}, (xmlhttp) => { getPopupCallback(xmlhttp, popupContentPath, title, finalCallback); });  
}


function getPopupCallback(xmlhttp, popupContentPath, title, finalCallback){
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {      
    let popupHTML = xmlhttp.responseText;        
    get(popupContentPath, {}, (xmlhttp) => { getPopupContentsCallback(xmlhttp, popupHTML, title, finalCallback) });
  } else if(xmlhttp.readyState == 4) {
    console.log(xmlhttp.responseText);
  }
}

function getPopupContentsCallback(xmlhttp, popupHTML, title, finalCallback){
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {      
    let popupContainerItem = document.getElementById("popup_container");
    popupContainerItem.innerHTML = popupHTML;
    let headingItem = document.getElementById("popup_heading")
    let popupContentItem = document.getElementById("popup_content")
    if (title != null) headingItem.innerHTML = title;
    else headingItem.remove();
    popupContentItem.innerHTML = xmlhttp.responseText;
    if (finalCallback) finalCallback();
  } else if(xmlhttp.readyState == 4) {
    console.log(this.responseText);
  }
}
var onPopupClosed = null;

function closePopup() {
  let popupContainerItem = document.getElementById("popup_container");
  if (popupContainerItem.children[0]){
    popupContainerItem.children[0].remove();  
    if (onPopupClosed) onPopupClosed();
    popupContainerItem.innerHTML = "";
  } 
}

function closeContext(){
  let contextContainerItem = document.getElementById("context_container");
  if (contextContainerItem.children[0]){
    contextContainerItem.children[0].remove();  
  } 
}

function onPopupBackgroundClicked(e){
  let sender = e.srcElement || e.target;
  if (event.target === event.currentTarget) {
    if (dismisPopupFunction) {
      dismisPopupFunction = dismisPopupFunction();
    } else {
      closePopup();
    }    
  }
}


function resetSideButtons(){
  let side_buttons = document.getElementsByClassName("side_button_selected");
  if (side_buttons.length == 0) return onNavClicked();
  for (let i in side_buttons){
    let side_button = side_buttons[i];
    if (side_button.classList != undefined) {
      let side_button_icon = side_button.getElementsByClassName("nav_icon")[0];
      let oldSrc = side_button_icon.src
      side_button_icon.src = side_button_icon.getAttribute("srcsel");
      side_button_icon.setAttribute("srcsel", oldSrc);
      side_button.classList.remove("side_button_selected");
    }    
  }
  onNavClicked();
}

function onNavClicked( ){
  for (let i in navClickedEventHandlers){
    navClickedEventHandlers[i]();
  }
}

function createDialog(type, title, message, confirmCallback, rejectCallback){
  createPopup("content/forms/popupDialog.html", title, () => {
    let confirmButton = document.getElementById("popupConfirmButton");
    let rejectButton = document.getElementById("popupRejectButton");
    let messageItem = document.getElementById("popUpMessage");
    if (typeof message == "string") messageItem.innerHTML = message;
    else if (message instanceof Element) messageItem.appendChild(message);
    confirmButton.onclick = confirmCallback;
    rejectButton.onclick = rejectCallback;
    
    switch (type){
      case DialogTypes.YesNo:
        confirmButton.value = "Yes";
        rejectButton.value = "No";
      break;
      case DialogTypes.OkCancel:
        confirmButton.value = "Ok";
        rejectButton.value = "Cancel";
      break;
      case DialogTypes.SaveCancel:
        confirmButton.value = "Save";
        rejectButton.value = "Cancel";
      break;
      case DialogTypes.Ok:
        confirmButton.value = "Ok";
        rejectButton.style.display = "none";
      break;
    }
  });
}  


function makeid(length) {
   let result           = '';
   let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   let charactersLength = characters.length;
   for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function isMobile(){
  if (mobile) return mobile.isMobile;
  mobile = { isMobile: false }
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  mobile.isMobile = check;
  return check;
}

function isIOS() {
  return [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform)
  // iPad on iOS 13 detection
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

function isNouveau(){
  if (window.location.pathname == "/nouveau.html"){
    return true;
  }
  return false;
}

var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

function replaceTag(tag) {
    return tagsToReplace[tag] || tag;
}

function replaceTags(str) {
  if (str == null) return str;    
  return str.replace(/[&<>]/g, replaceTag);
}

function isImageFileName(filename){
  filename = filename.toLowerCase();
  if (filename.indexOf("png") != -1) return true;
  if (filename.indexOf("jpg") != -1) return true;
  if (filename.indexOf("jpeg") != -1) return true;
  if (filename.indexOf("svg") != -1) return true;
  if (filename.indexOf("gif") != -1) return true;
  return false;
}

function getFileExtension(filename){
  let segs = filename.split('.');
  return segs[segs.length-1].toLowerCase();
}

function getIcon(extension){
  if (extension in fileIcons) return fileIcons[extension];
  return fileIcons["default"];
}

Date.prototype.getWeek = function() {
  let date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  let week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

Date.prototype.getHourMin = function() {
  var date = new Date(this.getTime());
  return date.toLocaleTimeString(navigator.language, {
    hour: '2-digit',
    minute:'2-digit'
  });
}

function imageFromClipboard(pasteEvent, callback){
	if(pasteEvent.clipboardData == false) return;    
  var items = pasteEvent.clipboardData.items;
  if(items == undefined) return;
  for (var i = 0; i < items.length; i++) {
    if (items[i].type.indexOf("image") == -1) continue;
    var blob = items[i].getAsFile();
    var mycanvas = document.createElement("canvas");
    var ctx = mycanvas.getContext('2d');    
    var img = new Image();
    img.onload = function(){
      mycanvas.width = this.width;
      mycanvas.height = this.height;
      ctx.drawImage(img, 0, 0);
      if(typeof(callback) == "function"){
        callback(mycanvas.toDataURL("image/png"));
      }
    }
    var URLObj = window.URL || window.webkitURL;
    img.src = URLObj.createObjectURL(blob);
  }
}

function createSimpleTable(data, direction, cssClass){
  let tableItem = document.createElement("table");
  tableItem.classList.add(cssClass) ;
  if (direction == Directions.Horizontal){
    for (let i in data){
      let rowData = data[i];
      let rowItem = document.createElement("tr");
      tableItem.appendChild(rowItem);
      let cellHeaderItem = document.createElement("th");
      cellHeaderItem.innerHTML = rowData[0];
      rowItem.appendChild(cellHeaderItem);
      for (let j = 1; j < rowData.length; j++){
        let cellItem = document.createElement("td");
        cellItem.innerHTML = rowData[j];
        rowItem.appendChild(cellItem);
      }
    }
  } else {
    let dataFound = true;
    let row = 0;
    while (dataFound) {
      dataFound = false;  
      let rowItem = document.createElement("tr");
      for (let col in data){
        let colData = data[col];
        let cellItem = document.createElement(row==0 ? "th" : "td");
        rowItem.appendChild(cellItem);
        if (colData.length > row){
          dataFound = true;
          cellItem.innerHTML = colData[row];  
        }
      }
      if (dataFound) tableItem.appendChild(rowItem);
      row ++;
    }
  }
  return tableItem;
}

/**
 * Static object containing filters for image manioulation and other tools
 */
var Filters = {
  /**
   * Helper function for getting the pixels from an image.
   *
   * @param {Image} img The image from which to get the pixels.
   * @param {dict} size Dict object with x and y component that indicates the desired 
   * size of the pixels array;
   * @return {Array} Array of pixels rgba values.
   */
  getPixels: (img, size = null) => {
    if (size === null) {
      size = {x: img.width, y: img.height};
    }
    let c = Filters.getCanvas(size.x, size.y);
    let ctx = c.getContext('2d');
    //ctx.clearRect(0,0,size.x, size.y);
    ctx.drawImage(img, 0, 0, size.x, size.y);
    return ctx.getImageData(0, 0, size.x, size.y);
  },
  /**
   * Helper function for making a canvas object.
   *
   * @param {number} w The width of the canvas.
   * @param {number} h The height of the canvas.
   * @return {Canvas} Canvas item with the desired dimensions.
   */
  getCanvas: (w, h) => {
    let c = document.createElement('canvas');
    c.width = w;
    c.height = h;
    return c;
  },
  filterImage: (image, filters, size = null) => {    
    if (size === null){
      size = {x: image.width, y: image.height};
    }
    let pixels = Filters.getPixels(image, size);
    for (var i = 0; i < filters.length; i++) {
      let filterFunction = filters[i].function;
      let parameters = filters[i].parameters;
      pixels = filterFunction(pixels, parameters);
    }
    return Filters.createImageFromPixels(pixels, size.x, size.y);
    //return pixels;    
  },
  grayscale: (pixels) => {
    let d = pixels.data;
    for (var i=0; i<d.length; i+=4) {
      let r = d[i];
      let g = d[i+1];
      let b = d[i+2];      
      let v = 0.2126*r + 0.7152*g + 0.0722*b;
      d[i] = d[i+1] = d[i+2] = v;
    }
    return pixels;
  },
  brightness: (pixels, adjustment, targetPixels = null) => {
    let d = pixels.data;    
    let td = targetPixels === null ? pixels.data : targetPixels.data;
    for (var i = 0; i < d.length; i += 4) {
      td[i] = d[i] + adjustment;
      td[i+1] = d[i+1] + adjustment;
      td[i+2] = d[i+2] + adjustment;
    }
    return targetPixels === null ? pixels : targetPixels;
  },
  brightnessRGB: (pixels, adjustment, targetPixels = null) =>{
    let d = pixels.data;    
    let td = targetPixels === null ? pixels.data : targetPixels.data;
    for (var i = 0; i < d.length; i += 4) {
      td[i] = d[i] + adjustment.r;
      td[i+1] = d[i+1] + adjustment.g;
      td[i+2] = d[i+2] + adjustment.b;
    }
    return targetPixels === null ? pixels : targetPixels;
  },
  contrast: (pixels, contrastAdjustment, saturationAdjustment, targetPixels = null) => {
    let d = pixels.data;
    let td = targetPixels === null ? pixels.data : targetPixels.data;
    for (var i = 0; i < d.length; i += 4) {
      let r = d[i];
      let g = d[i+1];
      let b = d[i+2];      
      let v = 0.2126*r + 0.7152*g + 0.0722*b;
      let sa = saturationAdjustment;
      let dr = sa*(v-r);
      let dg = sa*(v-g);
      let db = sa*(v-b);
      td[i] = contrastAdjustment*(d[i] + dr - 128) + 128;
      td[i+1] = contrastAdjustment*(d[i+1] + dg - 128) + 128;
      td[i+2] = contrastAdjustment*(d[i+2] + db - 128) + 128;
      
    }
    return targetPixels === null ? pixels : targetPixels;
  },
  tmpCanvas: document.createElement('canvas'),
  
  createImageData: function(w,h) {    
    return this.tmpCanvas.getContext('2d').createImageData(w,h);    
    //return ctx.createImageData(w,h);
  },

  createImageFromPixels: function(pixels, w, h) {
    let img = new Image();    
    let c = Filters.getCanvas(w, h);
    let ctx = c.getContext('2d');
    //ctx.clearRect(0,0,size.x, size.y);
    ctx.putImageData(pixels, 0, 0);
    img.src = c.toDataURL('image/png');
    return img;
  },

  convolute: function(pixels, weights, opaque) {
    let side = Math.round(Math.sqrt(weights.length));
    let halfSide = Math.floor(side/2);
    let src = pixels.data;
    let sw = pixels.width;
    let sh = pixels.height;
    // pad output by the convolution matrix
    let w = sw;
    let h = sh;
    let output = Filters.createImageData(w, h);
    let dst = output.data;
    // go through the destination image pixels
    let alphaFac = opaque ? 1 : 0;
    for (var y=0; y<h; y++) {
      for (var x=0; x<w; x++) {
        let sy = y;
        let sx = x;
        let dstOff = (y*w+x)*4;
        // calculate the weighed sum of the source image pixels that
        // fall under the convolution matrix
        let r=0, g=0, b=0, a=0;
        for (var cy=0; cy<side; cy++) {
          for (var cx=0; cx<side; cx++) {
            let scy = sy + cy - halfSide;
            let scx = sx + cx - halfSide;
            if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {
              let srcOff = (scy*sw+scx)*4;
              let wt = weights[cy*side+cx];
              r += src[srcOff] * wt;
              g += src[srcOff+1] * wt;
              b += src[srcOff+2] * wt;
              a += src[srcOff+3] * wt;
            }
          }
        }
        dst[dstOff] = r;
        dst[dstOff+1] = g;
        dst[dstOff+2] = b;
        dst[dstOff+3] = a + alphaFac*(255-a);
      }
    }
    return output;
  },
  screenFilter: (pixels) => {        
    let weight =  [ 1/9, 1/9, 1/9, 1/9, 1/9, 1/9, 1/9, 1/9, 1/9 ];
    pixels = Filters.brightness(pixels, 50);    
    pixels = Filters.convolute(pixels, weight);    
    let d = pixels.data;
    for (var i=0; i<d.length; i+=4) {
      let r = d[i];
      let g = d[i+1];      
      let b = d[i+2];  
      let a = Math.min(b, Math.min(r, g));      
      d[i+2] *= 0.7;
      d[i+3] = a;
    }
    return pixels;    
  },
  addAlphaMask: (pixels, alphaMask) => {
    let d = pixels.data;
    let ad = alphaMask.data;
    for (var i=0; i<d.length; i+=4) {
      let r = ad[i];
      let g = ad[i+1];      
      let b = ad[i+2];  
      let a = (r+g+b)/3;            
      d[i+3] = Math.min(a, d[i+3]);
    }
    return pixels;    
  },
  addAlphaMaskInv: (pixels, alphaMask, inverted = false) => {
    let d = pixels.data;
    let ad = alphaMask.data;
    for (var i=0; i<d.length; i+=4) {
      let r = ad[i];
      let g = ad[i+1];      
      let b = ad[i+2];  
      let a = (r+g+b)/3;            
      d[i+3] = 255-a;
    }
    return pixels;    
  },
  toBase64JPEG: (image, size = null, quality = 0.85) => {
    if (size === null){
      size = {x: image.width, y: image.height};      
    }
    let c = Filters.getCanvas(size.x, size.y);
    let context = c.getContext("2d");
    context.drawImage(image, 0, 0, size.x, size.y);
    return c.toDataURL("image/jpeg", quality);
  },
  toBase64PNG: (image, size = null) => {
    if (size === null){
      size = {x: image.width, y: image.height};      
    }
    let c = Filters.getCanvas(size.x, size.y);
    let context = c.getContext("2d");
    context.drawImage(image, 0, 0, size.x, size.y);		
    return c.toDataURL("image/png");
  }    
}

function TableCellItemHandler(tableRowData, cellIndex, type, placeHolder){
  this.cellItem = document.createElement("td");
  this.inputItem = null;
  this.type = type;
  switch (type){
    case "text":
      this.inputItem = document.createElement("input");  
      this.inputItem.placeholder = placeHolder;
      break;
    case "select":
      this.inputItem = document.createElement("select");   
      for (let i in placeHolder){
        let option = document.createElement("option");   
        option.innerHTML = placeHolder[i];
        option.value = i;
        this.inputItem.appendChild(option);
      }
      break;
  } 
  this.inputItem.classList.add("table_input");
  this.cellItem.appendChild(this.inputItem);
  this.myIntervalId = null;
  this.tableRowData = tableRowData; 
  this.cellIndex = cellIndex;  
  this.inputItem.type = type;

  if (tableRowData[cellIndex]) this.inputItem.value = tableRowData[cellIndex];
  this.inputItem.onchange = (e) => {
    this.handleChange();
  }

  this.handleChange = () => {
    if (this.type == "select") this.tableRowData[this.cellIndex] = parseInt(this.inputItem.value); 
    else this.tableRowData[this.cellIndex] = this.inputItem.value; 
    this.onChangedEvent(this);
  }
 
  this.onChangedEvent = (sender) => {
    console.log("TableCellItemHandler changed");
    console.log(sender);
    console.log(this.inputItem.value);
  }
}

function NamedInputHandler(inputDataDict, dataName, type, placeHolder, classes=null, inputItem=null){
  this.inputItem = inputItem;
  this.type = type;
  if (inputItem==null){
    switch (type){
      case "text":
        this.inputItem = document.createElement("input");  
        this.inputItem.placeholder = placeHolder;
        this.inputItem.type = type;
        break;
      case "checkbox":
        this.inputItem = document.createElement("input");  
        this.inputItem.type = type;
        break;
      case "select":
        this.inputItem = document.createElement("select");   
        for (let i in placeHolder){
          let option = document.createElement("option");   
          option.innerHTML = placeHolder[i];
          option.value = i;
          this.inputItem.appendChild(option);
        }
        break;
    } 
  }
  for (let i in classes)  
    this.inputItem.classList.add(classes[i]);

  this.myIntervalId = null;
  this.inputDataDict = inputDataDict; 
  this.dataName = dataName;  

  if (inputDataDict[dataName]) {
    if (type == "checkbox"){
      this.inputItem.checked = inputDataDict[this.dataName];
    } else {
      this.inputItem.value = inputDataDict[this.dataName];
    }
  }
    
  this.inputItem.onchange = (e) => {
    this.handleChange();
  }

  this.handleChange = () => {
    if (this.type == "select")  inputDataDict[this.dataName] = parseInt(this.inputItem.value); 
    else if (this.type == "checkbox")  inputDataDict[this.dataName] = this.inputItem.checked; 
    else  inputDataDict[this.dataName] = this.inputItem.value; 
    this.onChangedEvent(this);
  }
 
  this.onChangedEvent = (sender) => {
    console.log("TableCellItemHandler changed");
    console.log(sender);
    console.log(this.inputItem.value);
  }
}

function showQuill(name, path, quillSource, dataRoot=0){
  createPopup("/content/blogEditor.html", removeFileExtension(name), ()=>{
    document.getElementById("quill_editor");   
    let saveAndCloseButton = document.getElementById("save_note_button");

    let filepath = path + "/" + name;
    let solutionEditor = new Editor("quill_editor", null, dataRoot, filepath);
    solutionEditor.load();
    saveAndCloseButton.onclick = () => {
      solutionEditor.save();   
      closePopup();
    }    
    //solutionEditor.quill.setContents(JSON.parse(quillSource));
    /*document.getElementById("save_button").onclick = ()=>{
      console.log("save");
    }*/
  });
}

function showImage(name, imageSource){
  createPopup("/content/forms/imageView.html", name, ()=>{
    document.getElementById("image").src = imageSource;    
    document.getElementById("download_button").onclick = ()=>{
      var a = document.createElement("a"); 
      a.href = imageSource; 
      a.download = name; 
      a.click();
    }
  });
}

function showGuidance(sender){
  console.log(sender.getAttribute("path"));
  let path = "content/guidance/" + sender.getAttribute("path");
  let name = sender.getAttribute("name");
  createPopup(path, "Guidance", () =>{ });
}

function showExample (sender) {
  closePopup();
  let path = "content/guidance/" + sender.getAttribute("path");
  let name = sender.getAttribute("name");
  createPopup(path, "Example (Jambo)", () =>{ });
}

function showInfo(sender){  
  createPopup(sender.getAttribute("path") , "INFO", () =>{ });
}

function makeuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function insertCaptionItem(parent, caption, item){
  let captionItem = document.createElement("h4");
  captionItem.innerHTML = caption;
  captionItem.style = "margin-block-end: 0.0em; margin-left: 0.25em;"; 
  parent.appendChild(captionItem);
  parent.appendChild(item);
}

function insertCaptionValue(parent, caption, value){
  if (typeof(value) == "number") value = value.toString();
  if (typeof(value) !== "string") value = ""; 
  let captionItem = document.createElement("h4");
  let valueItem = document.createElement("div");
  captionItem.innerHTML = caption;
  captionItem.style = "margin-block-end: 0.0em; margin-left: 0.25em;";
  valueItem.classList.add("rounded_shade_value")  
  value = replaceTags(value);
  valueItem.innerHTML = value.replace(/\n/g, "<br/>");  
  parent.appendChild(captionItem);
  parent.appendChild(valueItem);
  return valueItem;
}

function onContextBackgroundClicked(e){
  let sender = e.srcElement || e.target;
  if (event.target === event.currentTarget) {
    closeContext();
  }
}

/**
 * Creates a context popup ui element and shows it on screen.
 *
 * @param {string} contextContentPath The path of the context menu html content.
 * @param {function} finalCallback Callback function to be called when the context menu is shown.
 * @return {null} 
 */
function createContext(contextContentPath, finalCallback){
  get(contextContentPath, {}, function() { 
    if (this.readyState == 4 && this.status == 200) {      
      let contextHTML = this.responseText;        
      let contextContainerItem = document.getElementById("context_container");
      contextContainerItem.innerHTML = contextHTML;
      finalCallback();
    } else if(this.readyState == 4) {
      console.log(this.responseText);
    }    
  });  
}

function createFileContextButton(onRenameClicked, onRemoveClicked) {
  let contextButton = document.createElement("div");
  let contextButtonIcon = new Image();
  contextButtonIcon.src = "icons/misc/context.svg";
  contextButtonIcon.classList.add("top_right_context_icon")
  contextButton.style.position = "relative";

  contextButton.appendChild(contextButtonIcon);
  contextButton.icon = contextButtonIcon;
  contextButton.onclick = (e) => {
    e.stopPropagation();
    let context = createContext("content/contextMenus/filesContextMenu.html", () =>{
      let contextMenu = document.getElementById("context_menu");
      let x = e.clientX -120;
      let y = e.clientY -50;
      contextMenu.style = "left: " + x + "px; top: " + y + "px;";
      let editButtonItem = document.getElementById("edit_context_menu_button");
      let removeButtonItem = document.getElementById("remove_context_menu_button");
      editButtonItem.onclick = (e) => { closeContext(); if (onRenameClicked) onRenameClicked(); }
      removeButtonItem.onclick = (e) => { closeContext(); if (onRemoveClicked) onRemoveClicked(); }
    });
  }
  return contextButton;
}

function createEditRemoveContextButton(onEditClicked, onRemoveClicked, white=false) {
  let contextButton = document.createElement("div");
  let contextButtonIcon = new Image();
  if (white){
    contextButtonIcon.src = "icons/misc/contextWhite.svg";
  } else {
    contextButtonIcon.src = "icons/misc/context.svg";
  }
  
  contextButtonIcon.classList.add("top_right_context_icon")
  contextButton.style.position = "relative";

  contextButton.appendChild(contextButtonIcon);
  contextButton.icon = contextButtonIcon;
  contextButton.onclick = (e) => {
    e.stopPropagation();
    let context = createContext("content/contextMenus/editRemoveContextMenu.html", () =>{
      let contextMenu = document.getElementById("context_menu");
      let x = e.clientX -120;
      let y = e.clientY -50;
      contextMenu.style = "left: " + x + "px; top: " + y + "px;";
      let editButtonItem = document.getElementById("edit_context_menu_button");
      let removeButtonItem = document.getElementById("remove_context_menu_button");
      editButtonItem.onclick = (e) => { closeContext(); if (onEditClicked) onEditClicked(); }
      removeButtonItem.onclick = (e) => { closeContext(); if (onRemoveClicked) onRemoveClicked(); }
    });
  }
  return contextButton;
}

function createContextButton(path, contextIdEvents) {
  let contextButton = document.createElement("div");
  let contextButtonIcon = new Image();
  contextButtonIcon.src = "icons/misc/context.svg";
  contextButtonIcon.classList.add("top_right_context_icon")
  contextButton.style.position = "relative";

  contextButton.appendChild(contextButtonIcon);
  contextButton.icon = contextButtonIcon;
  contextButton.onclick = (e) => {
    e.stopPropagation();
    let context = createContext(path, () =>{
      let contextMenu = document.getElementById("context_menu");
      let x = e.clientX -120;
      let y = e.clientY -50;
      contextMenu.style = "left: " + x + "px; top: " + y + "px;";
      for (let i in contextIdEvents){
        let contextIdEvent = contextIdEvents[i]; 
        let contextButtonItem = document.getElementById(i);
        if (contextButtonItem){
          contextButtonItem.onclick = (e) => { closeContext(); if (contextIdEvent) contextIdEvent(); }
        }
      }            
    });
  }
  return contextButton;
}

function onSendVerificationMail(){
  post("user/sendVerificationMail", {}, function() {
    if (this.readyState == 4 && this.status == 200) {
      let msg = "An email has been sent to your mailbox. Please open the mail and click the link to verify your account."
      createDialog(DialogTypes.Ok, "Account verification", msg, ()=>{ closePopup(); });
      console.log(this.responseText);
    } else if (this.readyState == 4){
      console.log(this.responseText);
    }
  });
}

function loadTextArea(textareaId, path){
  get(path, {}, function(){
    if (this.readyState == 4 && this.status == 200) {
      let textarea = document.getElementById(textareaId);
      textarea.value = this.responseText;
      //console.log(this.responseText);
    } else if (this.readyState == 4){
      console.log(this.responseText);
    }
  });
}

function loadImage(callback){
  var fileSelector = document.createElement('input');
  fileSelector.setAttribute('type', 'file');
  fileSelector.accept = "image/*";
  fileSelector.onchange = function (e) {
    handleFileSelect(e, callback);
  } 
  fileSelector.click();
}

function handleFileSelect(e, callback){
  var files = e.target.files;
  if (files.length < 1) {
    alert('select a file...');
    return;
  } else {
    var reader = new FileReader();    
    reader.onload = function(e) {
      let image = new Image();      
      image.onload = () => {
        callback(image);
      }
      image.src = e.target.result;
    }    
    reader.readAsDataURL(files[0]);
  }
}

function ImageFormatter(canvas, image) {
  this.canvas = canvas;
  this.ctx = canvas.getContext('2d');  
  this.c = Filters.getCanvas(this.canvas.width, this.canvas.height);    
  this.ctx1 = this.c.getContext('2d'); 
  this.alphaMask = Filters.getPixels(alphaMaskImage);
  this.useAlphaMask = true;
  this.image = image;
  this.offset = {x: 0, y: 0}
  this.residual = {x: 0, y: 0}
  this.minScale = Math.max(this.canvas.width/this.image.width, this.canvas.height/this.image.height);
  this.scale = this.minScale;  
  this.targetScale = this.minScale;
  this.moving = false;
  this.running = true;
  this.canvas.onmousedown = (e) => {
    //console.log("this.canvas.onmousedown");
    this.moving = true;
  }  
  this.canvas.onmouseup = (e) => {
    e.preventDefault();
    //console.log("this.canvas.onmousedown");
    this.moving = false;
  }
  this.canvas.onmousemove = (e) => {
    e.preventDefault();

    let clampx = this.scale * this.image.width - this.canvas.width;
    let clampy = this.scale * this.image.height - this.canvas.height;
    if (this.moving){
      this.offset.x += e.movementX;
      this.offset.y += e.movementY;
      this.offset.x = Math.min(Math.max(-clampx, this.offset.x), 0);
      this.offset.y = Math.min(Math.max(-clampy, this.offset.y), 0);
    }    
    
  }  
  this.canvas.onwheel = (e) => {
    if (e.deltaY > 0){
      this.targetScale /= 1.05;

    } else if (e.deltaY < 0) {
      this.targetScale *= 1.04;
    }

    this.targetScale = Math.max(this.minScale, this.targetScale);
  }
  this.render = () => {
    let w = this.image.width*this.scale;
    let h = this.image.height*this.scale;       
    this.ctx1.clearRect (0, 0, this.canvas.width, this.canvas.height);
    this.ctx1.drawImage(this.image, this.offset.x , this.offset.y, w, h);
    let pixels = null;
    if (this.useAlphaMask) pixels = Filters.addAlphaMask(this.ctx1.getImageData(0, 0, this.canvas.width, this.canvas.height), this.alphaMask);    
    else pixels = this.ctx1.getImageData(0, 0, this.canvas.width, this.canvas.height);
    
    this.ctx.putImageData(pixels, 0, 0);    
    //console.log("rendered" + this.canvas.width);
  } 
  this.getImage = () => {
    return this.canvas.toDataURL("image/jpeg", 0.85);
  }
  this.getPNGImage = () => {
    return this.canvas.toDataURL("image/png", 0.85);
  }
  this.stop = () => {
    this.running = false;
  }  
  this.mainLoop = () => {
    this.render();
    let deltaScale = (this.targetScale-this.scale)*0.3;    
    this.scale += deltaScale
    this.offset.x += ((this.offset.x-this.canvas.width/2)/this.scale)*deltaScale;
    this.offset.y += ((this.offset.y-this.canvas.height/2)/this.scale)*deltaScale;
    let clampx = this.scale * this.image.width - this.canvas.width;
    let clampy = this.scale * this.image.height - this.canvas.height;
    this.offset.x = Math.min(Math.max(-clampx, this.offset.x), 0);
    this.offset.y = Math.min(Math.max(-clampy, this.offset.y), 0);
    if (this.running){
      requestAnimationFrame(this.mainLoop);
    }    
  }
  this.mainLoop();
}

function onAvatarOkClicked(){
  console.log("onAvatarOkClicked")
  avatarHandler.stop();
  let avatarImageData = avatarHandler.getImage();  
  avatarHandler = null;    
  let avatarData = {
    image: avatarImageData
  }
  post("user/setAvatar", avatarData, function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log("avatar is set");    
      closePopup();
      location.reload();
    } else if (this.readyState == 4){
      console.log(this.responseText);
    }
  });
}

