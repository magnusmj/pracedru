document.addEventListener("DOMContentLoaded", function(){
  init();
});

function init(){
  onResumeClicked();
}

function onBlogClicked(){
  fillContent("content/blog.html", ()=>{
    get("blog/list", {}, getBlogListCallback, false);
  });  
}

function onResumeClicked(){
  fillContent("content/resume.html");  
}

function onPortfolioClicked(){
  fillContent("content/portfolio.html");
}


function onContactClicked() {
  fillContent("content/contact.html");
}

function getBlogListCallback(xmlhttp){
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {      
    let contentElement = document.getElementById("blog_content");
    let blogList = JSON.parse(xmlhttp.responseText);
    if (blogList.length != 0){
      contentElement.innerHTML = "";
    }
    for (let i in blogList) {    
      let blogItem = document.createElement("div");
      let blogEntry = blogList[i];
      blogItem.classList.add("content");
      blogItem.innerHTML = "<h1>" + blogEntry.title + "</h1>";
      blogItem.innerHTML += new Date(blogEntry.date);
      contentElement.appendChild(blogItem);
      
    }
  } else if(xmlhttp.readyState == 4){
    console.log(xmlhttp.responseText);
  }
}

function editBlog(blogId){
  createPopup("/content/blogEditor.html", removeFileExtension(name), ()=>{
    document.getElementById("quill_editor");   
    let saveAndCloseButton = document.getElementById("save_note_button");
    
    let blogEditor = new Editor("quill_editor", blogId);
    blogEditor.load();
    saveAndCloseButton.onclick = () => {
      blogEditor.save();   
      closePopup();
    }    
  });
}

function onCreateBlogClicked(){
  get("blog/create", { title: "New Blog" }, (xmlhttp) =>{
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {  
      let blogEntry = JSON.parse(xmlhttp.responseText); 
      console.log(blogEntry);
      editBlog(blogEntry.article);
    } else if(xmlhttp.readyState == 4) {
      console.log(xmlhttp.responseText);
    }
  });
  
}
