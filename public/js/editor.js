
function Editor(editorElementId, editorDataHolderId, config=null) {
  this.editorDataHolderId = editorDataHolderId;
  this.editorElement = document.getElementById(editorElementId);
  this.dataInfo = null;
  this.toolbarElement = null; //document.getElementById(toolbarElementId);
  this.parent = this.editorElement.parentElement;
  this.editorElement.style.minHeight = '250px';  
  this.messageElement = this.parent.querySelector('.message');
  this.quill = null;
  this.onSaved = null;
  if (typeof this.toolbarElement !== "undefined" && this.toolbarElement != null) {
    this.quill = new Quill(this.editorElement, {
      modules: {
        toolbar: {
          container: this.toolbarElement,          
        } 
      },    
      placeholder: 'Compose an epic...',
      theme: 'snow'
    });    
  } else if(config) {
    this.quill = new Quill(this.editorElement, {
      modules: {
        toolbar: [  
          [{ 'header': config.headers }],
          [{ 'size': config.sizes }],     
          ['bold', 'italic', 'underline', 'strike'],
          ['link', 'blockquote', 'code-block', 'image'],
          [{ list: 'ordered' }, { list: 'bullet' }]
        ]
      },    
      placeHolder: "Free text",
      theme: 'snow'
    });    
  } else {
    this.quill = new Quill(this.editorElement, {
      modules: {
        toolbar: [   
          [{ 'header': [1, 2, 3, 4, false] }],
          [{ 'size': ['small', false, 'large', 'huge'] }],     
          ['bold', 'italic', 'underline', 'strike'],
          ['link', 'blockquote', 'code-block', 'image'],
          [{ list: 'ordered' }, { list: 'bullet' }]
        ]
      },    
      placeHolder: "Free text",
      theme: 'snow'
    });
  }

  this.save = () => {
    let doc  = JSON.stringify(this.quill.getContents());  
    let dataHolder = {
      id: this.editorDataHolderId,
      data: doc
    }

    post("data/save", dataHolder, (xmlhttp) => { this.storeDataCallback(xmlhttp) });  
    if (this.onSaved) this.onSaved();
  }
  this.load = () => {
    let dataHolder = {
      id: this.editorDataHolderId,
    }
    get("data/item", dataHolder, (xmlhttp) => { this.getEditorDataCallback(xmlhttp); } ); 
  }
  this.disable = () => {
    this.quill.disable();
  }
  this.getEditorDataCallback = (xmlhttp) => {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      this.editorDataHolder = JSON.parse(xmlhttp.responseText);
      try {
        let editorData = JSON.parse(this.editorDataHolder.data);
        this.quill.setContents(editorData);
      } catch (e){
      
      }      
    } else if (xmlhttp.readyState == 4){
      console.log(xmlhttp.responseText);
    }
  } 
  this.storeDataCallback = (xmlhttp) => {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      console.log("document saved");
      if (this.editorDataHolder) this.editorDataHolder.__v++;
      let messageElement = this.messageElement;
      messageElement.innerHTML = "document saved";
      setTimeout(() => { messageElement.innerHTML = ""; }, 3000);
    } else if (xmlhttp.readyState == 4){
      console.log(xmlhttp.responseText);
    }
  }
}



