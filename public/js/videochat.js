"use strict";
document.addEventListener("DOMContentLoaded", function(){
  init();
});

let userData = null;
let signalChannel = null;
let rtcChannels = {};
let camStream = null;
let desktopStream = null;
let channelId = null;
let instanceId = null;
let channelName = "Meeting";
let localVideoPlayer = null;
let debug = false; // ulfert
let micOnly = false;
let momShown = false;
let notesShown = false;
let momEditor = null;
let notesEditor = null;

if (window.location.host.indexOf("localhost") != -1 || window.location.host.indexOf("dev.") != -1 ){
  debug = true;
}

const iceConf = {'iceServers': [
  { 
    'urls': 'turn:www.jambosystems.com:3478',
    'credential': 'st.alone',
    'username': 'rambo'
  }
]}

let captureConstraint = {
  audio: true,
  video: {
    facingMode: "user",
    frameRate: { ideal: 30 },
    width: {  ideal: 320, max: 320 },
    height: {  ideal: 240 }
  }
}

function init(){
  let urlParams = new URLSearchParams(window.location.search);
  let channelNameItem = document.getElementById("channel_name");
  userData = JSON.parse(localStorage.userData);
  channelId = urlParams.get('channelId');
  channelName = urlParams.get('channelName');
  micOnly = urlParams.get('micOnly')==="true";
  document.title = "Meeting " + channelName;
  channelNameItem.innerHTML = channelName;
  let ticket = localStorage.ticket;// userData._ticket
  startVideoStream();  
  get("user/userData", {}, onUserData, false);
  
}

function onUserData() {
  if (this.readyState == 4 && this.status == 200) {          
    userData = JSON.parse(this.responseText);
    if (userData.shownFirstUseGuides.indexOf(ViewTypes.VideoChat) == -1){
      startVideoChatTutorial();
    }
  } else if(this.status == 401){
    
  }
}

function startVideoChatTutorial(){
  get("content/tutorials/video.html", {}, function() { 
    if (this.readyState == 4 && this.status == 200) {      
      let popupContainerItem = document.getElementById("popup_container");
      popupContainerItem.innerHTML = this.responseText;               
    } else if(this.readyState == 4) {
      console.log(this.responseText);           
    }  
  });  

  post("/user/firstUseGuideShown", { guide: ViewTypes.VideoChat }, function (){
    if (this.readyState == 4 && this.status == 200) {   
      userData.shownFirstUseGuides.push(ViewTypes.VideoChat);                           
    } else if(this.readyState == 4) {
      userData.shownFirstUseGuides.push(ViewTypes.VideoChat);
      console.log(this.responseText);           
    }   
  });
}

function continueVideoTutorial1(){
  closePopup();

}

function initMomNotes(){
  let editorDataRoot = DataRoots.Meeting;
  let date = new Date(instanceId);
  let name = Date2String(date) + ".quill";
  let editorFileName = name;   
  momEditor = new Editor("momEditor", null, editorDataRoot, editorFileName);
  momEditor.dataInfo = channelId;
  momEditor.load();
  momEditor.onSaved = () => {
    signalChannel.send(JSON.stringify({ 't': MsgTypes.MOMSaved }));
  }
  editorDataRoot = DataRoots.User;
  editorFileName = "/tr4ns14t3-meetings";
  editorFileName += "/" + channelId;
  editorFileName += "/notes.quill";    
  notesEditor = new Editor("notesEditor", null, editorDataRoot, editorFileName);
  notesEditor.load();
}


function onMomTabClicked(){
  let momContainerItem = document.getElementById("mom_container");
  if (momShown){
    momContainerItem.style.animation = "momOut ease 0.5s";
    momContainerItem.style.right = "calc(0em - var(--mom-width))";
  } else {
    momContainerItem.style.animation = "momIn ease 0.5s";
    momContainerItem.style.right = "0em";
  }
  momShown = !momShown;
}

function onNotesTabClicked(){

  let notesContainerItem = document.getElementById("notes_container");
  if (notesShown){
    notesContainerItem.style.animation = "momOut ease 0.5s";
    notesContainerItem.style.right = "calc(0em - var(--mom-width))";
  } else {
    notesContainerItem.style.animation = "momIn ease 0.5s";
    notesContainerItem.style.right = "0em";
  }
  notesShown = !notesShown;
}


function RTCChannel (signalChannel, remoteUserId, caller=false) {
  this.remoteCamStream = null; //new MediaStream();
  this.remoteDesktopStream = null;
  this.signalChannel = signalChannel;
  this.remoteUserId = remoteUserId;
  this.peerConnection = null; //new RTCPeerConnection(iceConf);
  this.videoPlayer = null;
  this.caller = caller;
  this.remoteoa = null;
  this.desktopStreamSender = null;
  this.mediaConstraints = {
    'offerToReceiveAudio': true,
    'offerToReceiveVideo': true,
    'iceRestart': true    
  }
  
  this.addDesktopStream = () => {
    desktopStream.getTracks().forEach(track => {
      this.desktopStreamSender = this.peerConnection.addTrack(track, desktopStream);
    });
  }
  
  this.removeDesktopStream = () => {
    this.peerConnection.removeTrack(this.desktopStreamSender);
  }
  
  this.init = () => {
    this.remoteCamStream = new MediaStream();
    this.remoteDesktopStream = null;
    this.peerConnection = new RTCPeerConnection(iceConf);
    
    this.peerConnection.onicecandidate = this.onicecandidate;
    this.peerConnection.onnegotiationneeded = this.onnegotiationneeded;
    this.peerConnection.onconnectionstatechange = this.onconnectionstatechange;
    this.peerConnection.oniceconnectionstatechange = this.oniceconnectionstatechange;
    this.peerConnection.ontrack = this.ontrack;
    this.remoteoa = null;
    camStream.getTracks().forEach(track => {
      this.peerConnection.addTrack(track, camStream);
    });
    if (desktopStream){
      this.addDesktopStream();
    }
  }
  
  this.makeCall = async () => {
    try{
      let sdp = await this.peerConnection.createOffer(this.mediaConstraints);        
      await this.peerConnection.setLocalDescription(sdp);
      let desktopStreamId = null;
      if (desktopStream) desktopStreamId = desktopStream.id;
      let offerData = {
        't': MsgTypes.RTCChannelOffer,
        'senderUserId': userData._id,
        'recieverUserId': this.remoteUserId,
        'camStreamId': camStream.id,
        'desktopStreamId': desktopStreamId,
        'sdp': sdp
      }
      this.signalChannel.send(JSON.stringify(offerData));
    } catch (err) {
      console.log(err);
    }
    
  }  
  this.onAnswer = async (answer) => {
    this.remoteoa = answer;
    let remoteDesc = new RTCSessionDescription(answer.sdp);
    try {
      await this.peerConnection.setRemoteDescription(remoteDesc);
    } catch (err) {
      console.log(err);
    }
    
  }
  this.onOffer = async (offer) => {
    this.remoteoa = offer;
    this.peerConnection.setRemoteDescription(new RTCSessionDescription(offer.sdp));
    let sdp = await this.peerConnection.createAnswer();
    await this.peerConnection.setLocalDescription(sdp);
    let desktopStreamId = null;
    if (desktopStream) desktopStreamId = desktopStream.id;
    let answerData = {
      't': MsgTypes.RTCChannelAnswer,
      'senderUserId': userData._id,
      'recieverUserId': this.remoteUserId,
      'camStreamId': camStream.id,
      'desktopStreamId': desktopStreamId,
      'sdp': sdp
    }    
    this.signalChannel.send(JSON.stringify(answerData));

  }
  this.addIceCandidate = async (iceCandidate) => {
    try {
        await this.peerConnection.addIceCandidate(iceCandidate);
    } catch (e) {
        console.error('Error adding received ice candidate', e);
    }
  }
  this.close = () => {
    this.peerConnection.close();
    if (this.videoPlayer){
      this.videoPlayer.parentElement.remove(); 
      this.videoPlayer = null;
    }    
    delete rtcChannels[remoteUserId];
    if (this.remoteDesktopStream) {
      onDesktopShareStopped();
    }
  }
  this.onicecandidate = (e) => {
    if (e.candidate) {
      let iceCandidateData = {
        't': MsgTypes.ICECandidate,
        'senderUserId': userData._id,
        'recieverUserId': this.remoteUserId,
        'iceCandidate': e.candidate
      }
      this.signalChannel.send(JSON.stringify(iceCandidateData));
    } else {
    }
  }
  
  this.onnegotiationneeded = async (e) => {
    console.log("onnegotiationneeded");
    this.makeCall();
  }
  
  this.onconnectionstatechange = (e) => {
    let cs = this.peerConnection.connectionState;
    switch (cs) {
      case 'failed':
        setTimeout(this.reconnectFollowUp, 2000);
      break;
      default:
        console.log(cs);
    }
  }
  this.reconnectFollowUp = () => {
    let cs = this.peerConnection.connectionState;
    if (cs === 'failed'){
      this.makeCall();
    }  else {
      console.log (cs);
    }    
  }
  this.oniceconnectionstatechange = (e) => {
    let ics = this.peerConnection.iceConnectionState;
    switch (ics) {
      case 'connected':
        console.log("Peers connected");// Peers connected!
        if (this.videoPlayer == null){
          let videoPlayer = document.createElement("video");
          let contentItem = document.getElementById("video_container"); 
          let videoContainer = document.createElement("div");
          let controls_container = document.createElement("div");
          let controls = document.createElement("div");
          let speakerButton = createVideoControlButton("icons/channel/speaker.svg", "icons/channel/speakeroff.svg");
          
          controls_container.appendChild(controls);
          controls_container.style.position = "relative";
          controls.classList.add("video_controls")
          
          speakerButton.onclick = () => {
            let tracks = this.remoteCamStream.getAudioTracks();
            let enabled = true;      
            for (let i in tracks){
              tracks[i].enabled = !tracks[i].enabled;
              enabled = tracks[i].enabled;
            }
            speakerButton.switch(enabled);
          }
          
          controls.appendChild(speakerButton);
          videoContainer.appendChild(controls_container);
          
          
          videoContainer.classList.add("video_container");
          videoContainer.classList.add("zoom_in");
          videoPlayer.classList.add("video_chat");
          videoPlayer.classList.add("fade_in"); 
          videoPlayer.classList.add("zoom_in");    
          videoPlayer.setAttribute('playsinline', 'true');        
          videoPlayer.id = this.remoteUserId;
          videoContainer.appendChild(videoPlayer);
          contentItem.appendChild(videoContainer);
          this.videoPlayer = videoPlayer;
          videoPlayer.onloadedmetadata = e => videoPlayer.play();
        }
        this.videoPlayer.srcObject = this.remoteCamStream;

      break;
      case 'disconnected':  
        console.log("oniceconnectionstatechange disconnected");        
      break;
      case 'error':
        console.log("oniceconnectionstatechange error");
      break;
      case 'failed':
        console.log("oniceconnectionstatechange failed");        
        this.makeCall();
      break;
    }
  }
  
  this.ontrack = async (e) => {
    if (e.streams[0].id == this.remoteoa.camStreamId) {      
      this.remoteCamStream.addTrack(e.track, this.remoteCamStream);    
    } else if (e.streams[0].id == this.remoteoa.desktopStreamId) {
      console.log("desktop track added")
      this.remoteDesktopStream = new MediaStream();
      this.remoteDesktopStream.addTrack(e.track, this.remoteDesktopStream);    
      let contentItem = document.getElementById("desktop_container"); 
      try {            
        let videoPlayer = document.createElement("video");            
        let videoContainer = document.createElement("div");
        videoPlayer.classList.add("desktop_share");
        videoPlayer.classList.add("fade_in");
        //videoPlayer.setAttribute("controls","none")      
        videoPlayer.setAttribute('playsinline', 'true');
        videoContainer.appendChild(videoPlayer);
        contentItem.innerHTML = "";
        contentItem.appendChild(videoContainer);
        videoPlayer.srcObject = this.remoteDesktopStream;
        videoPlayer.onloadedmetadata = e => videoPlayer.play();     
        onDesktopShareStarted();
        if (desktopStream){
          let tracks = desktopStream.getTracks();
          for (let i in tracks){
            let track = tracks[i];
            track.stop();
          }
          desktopStream = null;
        }       
      } catch (e){
        console.log(e);
        contentItem.innerHTML = "<h1>Could not find webcam</h1>";
      }
    }   
  }
}

function SignalChannel() {
  let protocol = location.protocol === "https:" ? "wss://" : "ws://";
  let host = location.host;
  if (debug) {
    host = "localhost:10081";  
    protocol = "ws://";
  } 
  this.timerId = null;
  this.ws = new WebSocket(protocol + host + "/websocket");
  this.ws.onopen = async() => {
    let msg = {
      t: MsgTypes.SignalChannelRequest, 
      userId: userData._id,
      ticket: localStorage.ticket,      
      channelId: channelId
    }
    this.ws.send(JSON.stringify(msg));
    this.timerId = setInterval(this.keepAlive, 2000);
  }
  this.ws.onclose = (e) => {
    clearInterval(this.timerId);
    signalChannel = null;
    window.close();   
  }
  
  this.shareDesktop = () => {
    for (let i in rtcChannels){      
      if (rtcChannels[i]){
        rtcChannels[i].addDesktopStream();
      }
    }   
  }
  
  this.ws.onmessage = (e) => {
    let data = JSON.parse(e.data);
    let rtcChannel = null;
    switch (data.t) {
      case MsgTypes.ChannelData:
        for (let i in data.participants){
          let uid = data.participants[i];
          if (uid != userData._id){
            if (rtcChannels[data.senderUserId]){
              rtcChannels[data.senderUserId].close();
            }
            let rtcChannel = new RTCChannel(this, uid, true);
            rtcChannels[uid] = rtcChannel;
            rtcChannel.init();
          }
        }
        instanceId = data.instanceId;
        initMomNotes();   
      break;
      case MsgTypes.RTCChannelOffer:       
        if (rtcChannels[data.senderUserId]){
          rtcChannel = rtcChannels[data.senderUserId];
        } else {
          rtcChannel = new RTCChannel(this, data.senderUserId);
          rtcChannel.init()
        }
        
        rtcChannels[data.senderUserId] = rtcChannel;
        rtcChannel.onOffer(data);
      break;
      case MsgTypes.RTCChannelAnswer:
        rtcChannel = rtcChannels[data.senderUserId];
        if (rtcChannel) rtcChannel.onAnswer(data);
      break;
      case MsgTypes.ICECandidate:
        rtcChannel = rtcChannels[data.senderUserId];
        if (rtcChannel) rtcChannel.addIceCandidate(data.iceCandidate);
      break;
      case MsgTypes.UserLeft:
        rtcChannel = rtcChannels[data.userId];
        delete rtcChannels[data.userId];
        if (rtcChannel) rtcChannel.close();
        
      break;  
      case MsgTypes.DesktopShareStopped:
        onDesktopShareStopped();
      break;
      case MsgTypes.MOMSaved:
        setTimeout(()=>momEditor.load(), 500);
      break;
    }
  }
  
  this.ws.onerror = (evt) => {
  	console.log(evt);
  }
  this.send = (msg) => {
    this.ws.send(msg);
  }
  this.keepAlive = () => {
    this.ws.send(JSON.stringify({ t: MsgTypes.Ping }));
  }
  this.onWebRTCConnectionFailed = (wrtcc) => {
    if (wrtcc.remoteUserId in rtcChannels){      
      wrtcc.close();   
      if (wrtcc.caller == true){
        let rtcChannel = new RTCChannel(this, wrtcc.remoteUserId, true);
        rtcChannels[wrtcc.remoteUserId] = rtcChannel;      
      }      
    }    
  }
  this.close = () => {
    this.ws.close();
  }
}

function onDesktopShareStarted(){
  let videoContainerItem = document.getElementById("video_container")
    videoContainerItem.classList.remove("fade_in");
  videoContainerItem.classList.add("fade_out");
}

function onDesktopShareStopped(){
  let videoContainerItem = document.getElementById("video_container")
  videoContainerItem.classList.remove("fade_out");
  videoContainerItem.classList.add("fade_in");
  let contentItem = document.getElementById("desktop_container"); 
  contentItem.innerHTML = "";
  document.getElementById("share_desktop_button").value = "Share desktop";
  for (let i in rtcChannels){      
    if (rtcChannels[i]){
      rtcChannels[i].remoteDesktopStream = null;
    }
  } 
}

async function startDesktopStream(sender){
  let contentItem = document.getElementById("desktop_container"); 
  if (!desktopStream) {    
   
    try {
      let displayMediaOptions = {
        video: {
          cursor: "always"
        },
        audio: false
      }
      desktopStream = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
      sender.value = "Stop sharing";
      let videoPlayer = document.createElement("video");    
      let videoContainer = document.createElement("div");
      videoPlayer.classList.add("desktop_share");
      videoPlayer.classList.add("fade_in");
      //videoPlayer.setAttribute("controls","none")      
      videoPlayer.setAttribute('playsinline', 'true');
      contentItem.innerHTML = "";
      videoContainer.appendChild(videoPlayer);
      contentItem.appendChild(videoContainer);
      videoPlayer.srcObject = desktopStream;
      videoPlayer.onloadedmetadata = e => videoPlayer.play();
      videoPlayer.muted = true;
      
      signalChannel.shareDesktop(desktopStream);
      onDesktopShareStarted();
      
      desktopStream.getTracks()[0].onended = function () {
        startDesktopStream(sender);
      };
    } catch (err) {
      console.log(err);
    }
  } else {  
    sender.value = "Share desktop";
    if (desktopStream){
      for (let i in rtcChannels){      
        if (rtcChannels[i]){
          rtcChannels[i].removeDesktopStream();
        }
      }   
    
      let tracks = desktopStream.getTracks();
      for (let i in tracks){
        let track = tracks[i];
        track.stop();
      }
      desktopStream = null;
      let msg = {
        t: MsgTypes.DesktopShareStopped, 
        userId: userData._id,
        ticket: localStorage.ticket,      
        channelId: channelId
      }
      signalChannel.send(JSON.stringify(msg));
    }    
    onDesktopShareStopped();
  }
}
 
function createVideoControlButton(srcon, srcoff){
  let buttonItem = document.createElement("div");
  let iconItem = new Image();// document.createElement("img");
  iconItem.src = srcon;
  iconItem.style.height = "2em";
  buttonItem.classList.add("video_control_button")
  buttonItem.appendChild(iconItem);
  buttonItem.setAttribute("srcon", srcon);
  buttonItem.setAttribute("srcoff", srcoff);
  buttonItem.switch = (value) => {
    iconItem.src = value ? srcon : srcoff;
  }
  return buttonItem;
} 

async function startVideoStream(){  
  let contentItem = document.getElementById("video_container"); 
  try {    
    if (micOnly) {
      captureConstraint.video = false;
    }
    camStream = await navigator.mediaDevices.getUserMedia(captureConstraint);
    let videoPlayer = document.createElement("video");    
    let videoContainer = document.createElement("div");
    let controls_container = document.createElement("div");
    let controls = document.createElement("div");
    let micButton = createVideoControlButton("icons/channel/mic.svg", "icons/channel/micoff.svg");
    let camButton = createVideoControlButton("icons/channel/cam.svg", "icons/channel/camoff.svg");
    controls_container.appendChild(controls);
    controls_container.style.position = "relative";
    controls.classList.add("video_controls")
    
    micButton.onclick = () => {
      let tracks = camStream.getAudioTracks();
      let enabled = true;      
      for (let i in tracks){
        tracks[i].enabled = !tracks[i].enabled;
        enabled = tracks[i].enabled;
      }
      micButton.switch(enabled);
    }
    camButton.onclick = () => {
      let tracks = camStream.getVideoTracks();
      let enabled = true;
      for (let i in tracks){
        tracks[i].enabled = !tracks[i].enabled;
        enabled = tracks[i].enabled;
      }      
      camButton.switch(enabled);
    }
    controls.appendChild(micButton);
    if (!micOnly) controls.appendChild(camButton);
    videoContainer.appendChild(controls_container);

    videoContainer.classList.add("video_container");
    videoContainer.classList.add("zoom_in");
    videoPlayer.classList.add("video_chat");
    videoPlayer.classList.add("fade_in");
    //videoPlayer.setAttribute("controls","controls")      
    videoPlayer.setAttribute('playsinline', 'true');
    videoContainer.appendChild(videoPlayer);
    contentItem.innerHTML = "";
    contentItem.appendChild(videoContainer);
    videoPlayer.srcObject = camStream;
    videoPlayer.onloadedmetadata = e => videoPlayer.play();
    videoPlayer.muted = true;
    localVideoPlayer = videoPlayer;
    signalChannel = new SignalChannel(camStream);
   
  } catch (e){
    console.log(e);
    contentItem.innerHTML = "<h1>Could not find webcam</h1>";
  }
}

function closeVideoChat(){
  if (signalChannel){
    signalChannel.close();
    signalChannel = null;
  }  
  for (let i in rtcChannels){
    rtcChannels[i].close();
    delete rtcChannels[i];
  }
  if (camStream){
    camStream.getTracks().forEach(function(track) {
      track.stop();
    });
    camStream = null;
  }
  window.close();    
}
