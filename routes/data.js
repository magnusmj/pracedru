const express = require('express')
const router = express.Router()
const DataHolder = require("../models/dataholder");

router.get('/item', async (req, res, next) =>{
  let dataHolder = await DataHolder.findById(req.query.id);
  if (dataHolder){
    if (dataHolder.data) res.send(dataHolder.data.toString("utf8"));
    else res.send("");
  } 
  else console.log("dataHolder not found");
});

router.post('/save', async (req, res, next) => {
  console.log(req.body);
  let dataHolder = await DataHolder.findById(req.body.id)
  dataHolder.data = req.body.data;
  await dataHolder.save();  

  res.send("saved");
});


module.exports = router
