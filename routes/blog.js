const express = require('express')
const router = express.Router()
const Blog = require("../models/blog");
const DataHolder = require("../models/dataholder");

router.get('/list', async(req, res, next) =>{
  let blogs = await Blog.find({ "published": true }).sort("-date").limit(10).exec();
  res.send(JSON.stringify(blogs));
});

router.get('/create', async(req, res, next) => {
  let dataHolder = new DataHolder();
  await dataHolder.save();
  let blogData = {
    title: "New Entry",
    date: Date.now(),
    snippet: "blank",
    hashtags: [],
    article: dataHolder._id    
  }
  let blogEntry = new Blog(blogData);
  await blogEntry.save();
  res.send(JSON.stringify(blogEntry));
});

router.get("/entry", async(req, res, next) => {
  let blogEntry = await Blog.findById(req.query.id);
  if (blogEntry) {
    blogEntry.populate("article").execPopulate();
    res.send(JSON.stringify(blogEntry));
  }
});


module.exports = router
