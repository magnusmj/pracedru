#!/usr/bin/env node

const fs = require('fs');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

var configfile = "config.json";

for (let i in process.argv){
  var arg = process.argv[i];
  if (arg.includes("cfg=")  || arg.includes("config=") || arg.includes("configfile="))
    configfile = arg.split("=")[1];
}

let config = null;

try {
  if (fs.existsSync(configfile)){
    var data = fs.readFileSync(configfile, 'utf8')
    config = JSON.parse(data);  
  }
} catch (e){
  console.log('config file ' + configfile + ' not found');  
  console.log(e);
}

if (config == null) process.exit(5);

console.log(config.port);

mongoose.connect("mongodb://localhost/" + config.dbname, config.mongoConfig);
var app = express();

var server = app.listen(config.port, function () {
  console.log('Example app listening on port ' + config.port)
})

app.use('/blog', require("./routes/blog"))
app.use('/data', require("./routes/data"))

app.use(express.static('./public'));
